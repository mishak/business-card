---
title: Michal Gebauer - DevOps Consultant
navbar: false
# http://microformats.org/wiki/h-card
# https://schema.org/Person
# https://search.google.com/structured-data/testing-tool#
# https://pin13.net/mf2/
---

![TechMeetup 2019](./techmeetup-2019.jpg)

<div class="h-card vcard" itemscope itemtype="https://schema.org/Person">

<h1 itemprop="name">
<a href="https://michalgebauer.com" class="p-name fn url" itemprop="url">
<span class="p-given-name" itemprop="givenName">Michal</span>
<span class="p-family-name" itemprop="familyName">Gebauer</span>
</a>
</h1>

<h2 class="p-job-title title" itemprop="jobTitle">DevOps Consultant</h2>

<a href="mailto:contact@michalgebauer.com" class="p-email email" itemprop="email" content="contact@michalgebauer.com">contact@michalgebauer.com</a>

<a itemprop="sameAs" href="https://linkedin.com/in/michalgebauer/">linkedin.com/in/michalgebauer</a>

</div>

<script>
export default {
    created () {
        // add link to microformat hcard profile
        if (typeof this.$ssrContext !== 'undefined') {
            this.$ssrContext.userHeadTags += '<link rel="profile" href="http://microformats.org/profile/hcard"/>';
        }
    }
}
</script>
